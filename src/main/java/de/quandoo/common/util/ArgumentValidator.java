package de.quandoo.common.util;

import static de.quandoo.recruitment.common.contants.QuandooConst.INVALID_CUISINE_ARGUMENT;
import static de.quandoo.recruitment.common.contants.QuandooConst.INVALID_CUISINE_NAME;
import static de.quandoo.recruitment.common.contants.QuandooConst.INVALID_CUSTOMER_ARGUMENT;
import static de.quandoo.recruitment.common.contants.QuandooConst.INVALID_USER_ID;
import static de.quandoo.recruitment.common.contants.QuandooConst.SUPPRESS_DEFAULT_CONSTRUCTOR_FOR_NON_INSTANTIABILITY;

import de.quandoo.exception.QuandooException;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public final class ArgumentValidator {

    private ArgumentValidator() {
        throw new AssertionError(SUPPRESS_DEFAULT_CONSTRUCTOR_FOR_NON_INSTANTIABILITY);
    }

    public static void validateParams(Customer userId, Cuisine cuisine) throws QuandooException {
        validateCustomer(userId);
        validateCuisine(cuisine);
    }

    public static void validateCuisine(Cuisine cuisine) throws QuandooException {
        if (cuisine == null) {
            throw new IllegalArgumentException(INVALID_CUISINE_ARGUMENT);
        }
        if (null == cuisine.getName()) {
            throw new QuandooException(INVALID_CUISINE_NAME);
        }
    }

    public static void validateCustomer(Customer userId) throws QuandooException {
        if (userId == null) {
            throw new IllegalArgumentException(INVALID_CUSTOMER_ARGUMENT);
        }
        if (null == userId.getUuid()) {
            throw new QuandooException(INVALID_USER_ID);
        }
    }
}
