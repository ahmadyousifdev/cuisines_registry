package de.quandoo.recruitment.common.contants;

public interface QuandooConst {

    String INVALID_CUISINE_NAME = "Invalid Cuisine name";
    String INVALID_USER_ID = "Invalid user id";
    String INVALID_CUSTOMER_ARGUMENT = "Invalid customer argument";
    String INVALID_CUISINE_ARGUMENT = "Invalid Cuisine argument";
    String SUPPRESS_DEFAULT_CONSTRUCTOR_FOR_NON_INSTANTIABILITY = "Suppress default constructor for non-instantiability";
}
