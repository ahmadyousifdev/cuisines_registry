package de.quandoo.recruitment.registry;

import static org.hamcrest.core.IsEqual.equalTo;

import de.quandoo.recruitment.common.contants.CuisineType;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import java.util.List;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

public class InMemoryCuisinesRegistryTest {

    private static final String ONE_STRING = "1";
    private static final String TWO_STRING = "2";
    private static final String THREE_STRING = "3";

    private InMemoryCuisinesRegistry cuisinesRegistry;

    @Rule
    public final ErrorCollector collector = new ErrorCollector();

    @Before
    public void setUp() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();
    }

    @Test
    public void givenCuisineCustomersWhenFrenchExistsThenReturnsList() {
        // Arrange
        arrangeRegister(CuisineType.FRENCH.name(), CuisineType.GERMAN.name(), CuisineType.ITALIAN.name());

        // Act
        List<Customer> actual = cuisinesRegistry.cuisineCustomers(new Cuisine(CuisineType.FRENCH.name()));

        // Assert
        collector.checkThat(actual.size(), equalTo(1));
    }

    @Test
    public void givenCuisineCustomersWhenFrenchMoreElementExistsThenReturnsList() {
        // Arrange
        arrangeRegister(CuisineType.FRENCH.name(), CuisineType.FRENCH.name(), CuisineType.ITALIAN.name());

        // Act
        List<Customer> actual = cuisinesRegistry.cuisineCustomers(new Cuisine(CuisineType.FRENCH.name()));

        // Assert
        collector.checkThat(actual.size(), equalTo(2));
    }

    @Test
    public void givenCuisineCustomersWhenCuisineNullThenReturnsZero() {
        // Act
        List<Customer> actual = cuisinesRegistry.cuisineCustomers(null);

        // Assert
        collector.checkThat(actual.size(), equalTo(0));
    }

    @Test
    public void givenCustomerCuisinesWhenCustomerNullThenReturnsZero() {
        // Act
        List<Cuisine> actual = cuisinesRegistry.customerCuisines(null);

        // Assert
        collector.checkThat(actual.size(), equalTo(0));
    }

    @Test
    public void givenTopCuisinesWhenNoElementsThenReturnsZero() {
        // Act
        List<Cuisine> actual = cuisinesRegistry.topCuisines(1);

        // Assert
        collector.checkThat(actual.size(), equalTo(0));
    }

    @Test
    public void givenTopCuisinesWhenFrenchMoreElementExistsThenReturnsList() {
        // Arrange
        arrangeRegister(CuisineType.FRENCH.name(), CuisineType.FRENCH.name(), CuisineType.ITALIAN.name());

        // Act
        List<Cuisine> actual = cuisinesRegistry.topCuisines(2);

        // Assert
        collector.checkThat(actual.size(), equalTo(2));
        collector.checkThat(actual.get(0).getName(), equalTo(CuisineType.FRENCH.name()));
        collector.checkThat(actual.get(1).getName(), equalTo(CuisineType.ITALIAN.name()));
    }

    @Test
    public void givenTopCuisinesWhenParamExceedExistingThenReturnsList() {
        // Arrange
        arrangeRegister(CuisineType.FRENCH.name(), CuisineType.FRENCH.name(), CuisineType.ITALIAN.name());

        // Act
        List<Cuisine> actual = cuisinesRegistry.topCuisines(3);

        // Assert
        collector.checkThat(actual.size(), equalTo(2));
        collector.checkThat(actual.get(0).getName(), equalTo(CuisineType.FRENCH.name()));
        collector.checkThat(actual.get(1).getName(), equalTo(CuisineType.ITALIAN.name()));
    }

    private void arrangeRegister(String cuisine1, String cuisine2, String cuisine3) {
        cuisinesRegistry.register(new Customer(ONE_STRING), new Cuisine(cuisine1));
        cuisinesRegistry.register(new Customer(TWO_STRING), new Cuisine(cuisine2));
        cuisinesRegistry.register(new Customer(THREE_STRING), new Cuisine(cuisine3));
    }
}
